import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)


import CookieInfo from '~/components/cookie-info.vue'
Vue.component('CookieInfo', CookieInfo)
